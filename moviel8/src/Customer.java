
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Vector;

public class Customer {

	private String name;
	private Vector rental = new Vector();

	public Customer(String name) {
		name = name;
	}

	public void addRental(Rental arg) {
		rental.addElement(arg);
	}

	public String getName() {
		return name;
	}
	
	public String statement() {
		final Enumeration<Rental> rentals = rental.elements();
		String result = "Rental record for " + getName() + "\n";
		while (rentals.hasMoreElements()) {
			final Rental each = rentals.nextElement();
			result += "\t" + each.getMovie().getTitle() + "\t"
					+ String.valueOf(each.getRentalAmount()) + "\n";
		}
		result += "Amount owed is " + String.valueOf(charge()) + "\n";
		result += "You earned " + String.valueOf(frequentRenterPoints())
				+ " frequent renter points";
		return result;
	}

	private int frequentRenterPoints() {
		int result = 0;
		final Enumeration<Rental> rentals = rental.elements();
		while (rentals.hasMoreElements()) {
			final Rental each = rentals.nextElement();
			result += each.frequentRenterPointsOf();
		}
		return result;
	}

	private double charge() {
		double result = 0;
		final Enumeration<Rental> rentals = rental.elements();
		while (rentals.hasMoreElements()) {
			final Rental each = rentals.nextElement();
			result += each.getRentalAmount();
		}
		return result;
	}
}
